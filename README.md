# TPs Sécurité des objets connectés - Igor Robin - ZZ2 F1

Dans ce dépôt figurent :
- Une documentation sur les TP 1 et 2
- Le code source Python
- Ce README
- Une capture d'écran du HDD Hex Editor montrant ce qui a pu être extrait des fichiers avant de les télécharger en entier

Afin de pouvoir lancer le code:
- Installer python3.10
- Installer bleak
- Activer le Bluetooth sur son ordinateur
- Allumer le boîtier et appuyer sur le bouton jusqu'à ce qu'il fasse un bip et que les 3 lumières s'allument
- Dans le repository, faire python TP1.py dans PowerShell (le fichier s'appelle TP1 mais contient toutes le code pour les 2 TPs)
