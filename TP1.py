# Le fait d'ecrire dans des fichiers separe n'as pas ete implemente, l'idee serait de reperer dans le fichier
# concatenant toutes les donnees les fins de fichiers et de separer les donnees puis de decoder les fichiers

# On importe les bibliotheques
import asyncio
from bleak import BleakScanner
from bleak import BleakClient
from bleak.backends.characteristic import BleakGATTCharacteristic

# On scanne l'environnement et on affiche tous les appareils presents
async def scan():
    devices = await BleakScanner.discover()
    for d in devices:
        print(d)

print("DEBUT DU SCAN")
asyncio.run(scan())
print("FIN DU SCAN")

# Adresse MAC du Dwilen
addresse_mac_dwilen = "CF:5F:30:C9:CA:37"

# Liste de toutes les handles et caracteristiques
handles = []
characteristics = []

# UUID utile
MODEL_NBR_UUID = 120

# Informations utiles
NOM_FICHIER_1 = '5431300000'
NOM_FICHIER_2 = '5431310000'
NOM_FICHIER_3 = '5431330000'
NOM_FICHIER_4 = '5232333937'
DONNEES_FIN = '0000000000000009090909'
ATTENTE = 30

def callback_fichier(sender: BleakGATTCharacteristic, data: bytearray):
    fic = open('les_4_fichiers_a_la_suite', 'ab+')
    if (fic):
        fic.write(data)
        print(f"{sender}: {data.hex()}")

def callback_classique(sender: BleakGATTCharacteristic, data: bytearray):
    print(f"{sender}: {data.hex()}")

async def lister_caracteristiques(addresse_mac_dwilen, timeout=30.0):
    async with BleakClient(addresse_mac_dwilen) as client:
        services = client.services
        for s in services:
            print(s)
            for c in s.characteristics:
                print(c)
                characteristics.append(c)
                handles.append(c.handle)
            print("\n")
        print("NUMERO DE MODELE")
        model_number = await client.read_gatt_char(MODEL_NBR_UUID)
        print(model_number)

print("DEBUT LISTAGE CARACTERISTIQUES")
asyncio.run(lister_caracteristiques(addresse_mac_dwilen))
print("FIN LISTAGE CARACTERISTIQUES")

print("LISTE DE TOUTES LES HANDLES ASSOCIEES AUX CARACTERISTIQUES")
print(handles)

async def tout_lire_et_ecrire(address, timeout=30.0):
    async with BleakClient(address) as client:

        ack = await client.start_notify(100, callback_classique)
        log = await client.start_notify(111, callback_classique)
        fichier = await client.start_notify(66, callback_fichier)
        liste = await client.start_notify(58, callback_classique)

        for i in range(len(handles)):
            try:
                info = await client.read_gatt_char(handles[i])
                print(characteristics[i])
                print(info)
            except:
                print("CARACTERISTIQUE NON EN LECTURE")

        num = await client.read_gatt_char(62)
        liste = await client.write_gatt_char(58, num)  
        print(liste)

        await asyncio.sleep(ATTENTE)
        print("ECRITUE PREMIER FICHIER")
        lu = await client.write_gatt_char(66, bytearray.fromhex(NOM_FICHIER_1 + DONNEES_FIN))
        await asyncio.sleep(ATTENTE)
        print("ECRITURE DEUXIEME FICHIER")
        lu = await client.write_gatt_char(66, bytearray.fromhex(NOM_FICHIER_2 + DONNEES_FIN))
        await asyncio.sleep(ATTENTE)
        print("ECRITURE TROISIEME FICHIER")
        lu = await client.write_gatt_char(66, bytearray.fromhex(NOM_FICHIER_3 + DONNEES_FIN))
        await asyncio.sleep(ATTENTE)
        print("ECRITURE QUATRIEME FICHIER")
        lu = await client.write_gatt_char(66, bytearray.fromhex(NOM_FICHIER_4 + DONNEES_FIN))
        await asyncio.sleep(ATTENTE)

print("DEBUT DE LA PRODCEDURE POUR ECRIRE LES FICHIERS")
asyncio.run(tout_lire_et_ecrire(addresse_mac_dwilen))
print("FIN")
